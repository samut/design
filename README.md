<blockquote class="twitter-tweet" lang="en">
<p lang="en" dir="ltr">i want computers to be as magical as their
promises</p>&mdash; The Wrath of me™ (@hirojin) <a href="https://twitter.com/hirojin/status/628115315801608192">August 3,2015</a>
</blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Somewhere along the way we appear to [have forgotten that computers are
actually magical.](https://twitter.com/hirojin/status/645645981791485952)

I, like [Reginald Braithwaite](https://twitter.com/raganwald), [have a good
feeling about this](https://vimeo.com/76141334), though.


------


Modern computer systems have gotten extremely complex because the only way to
scale them to the desired numbers is to run every each component on different
machines. But we are building these complex systems on top of a legacy of 40
years of abstractions and hacks and backwards compatibilities to those hacks no
one even remembers anymore.

These [complex systems fail in unforseen ways](https://www.youtube.com/watch?v=ieCTIPG43no).

With [*samut*](https://gitlab.com/groups/samut) I would like to bring back our
belief in magic. I'm convinced the only way to do that is by getting rid of the
illusions.

* [No one cares that it works on your machine](https://blog.codinghorror.com/the-works-on-my-machine-certification-program/)
* [The network is *not* reliable](http://queue.acm.org/detail.cfm?id=2655736)
* [Our code and its security has real effect on real people](http://swiftonsecurity.tumblr.com/post/98675308034/a-story-about-jessica)

And accepting the realities

* [We have to embrace distributed systems.](https://www.sics.se/~joe/thesis/armstrong_thesis_2003.pdf)
* [We have to embrace that complex systems will fail.](http://web.mit.edu/2.75/resources/random/How%20Complex%20Systems%20Fail.pdf)
* [We'll need deep introspection](https://queue.acm.org/detail.cfm?id=1117401).
* [We have to make simple things easy](http://www.infoq.com/presentations/Simple-Made-Easy)

Only then will we be able to *reliably* build the *complex systems* that this
*complex world* needs.


-----

## Goals

<blockquote class="twitter-tweet" lang="en">
<p lang="en" dir="ltr">Engineering without Craft is brittle; without Science
inconsequential; without Art barbarous</p>&mdash; David Schmitt (@dev_el_ops)
<a href="https://twitter.com/dev_el_ops/status/645557403203817472">September 20, 2015</a>
</blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

We should also remember our history. Our craft is filled with good examples to
follow and bad ones to avoid. However, while I'm convinced it's necessary to
state a projects goals very clearly, I believe that one shouldn't confine them
too strictly. Goals should be ambitious.

### Libraries as a Service

Samut is a way to connect different programming languages over the network.

This allows us to transcend machine, and operating system boundaries.
It allows us to isolate fragile parts away from critical components.
We can *let it crash* and it won't affect *our* program. We can also recover,
retry, or retry on a different node - automatically.


### Zero Click Deployment

Samut is a way to build and deploy complex systems.

We will guarantee the same system works the same in different environments, by
first and foremost ensuring that it's the same system.

The way to contain the complexity is to make the infrastructure immutable. The
moving parts should be able to interconnect - through auto-configuration - with
only minimal intervention from operators.


### 100% Transparency

Samut is a way to debug complex systems.

Operators are first-class citizens on the system. Introspection is so simple
that we don't have to rely on logs.
